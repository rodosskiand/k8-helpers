#!/bin/bash

#delete images 
function tags(){
	allTags=$(gcloud container images list-tags $1 --limit=unlimited --format='get(tags)' --filter='tags:*'| tr ';' '\n' | awk "NR>$2")
	if [ ${#allTags[0]} != 0 ] ; then printf '%s\n' "${allTags[@]}" | parallel "gcloud container images delete $1:{1} --force-delete-tags" ; fi
}

#delete untagged images
function del_untagged(){
	for image in $(gcloud container images list-tags $1 --limit=unlimited --format='get(digest)' --filter='-tags:*'| tr ';' '\n');
		do (gcloud container images delete $1@${image})
	done
}

export -f tags
export -f del_untagged

re='^[0-9]+$' 

start=$SECONDS

#add images
images=$(gcloud container images list --format='get(name)')

# add subdirectories
IFS=' ' result=$(printf '%s\n' "${images[@]}" | parallel   "gcloud container images list --repository={1} --format='get(name)'") 

# add all subdirectories
while [[ ! -z $result ]]; do
	sumResult=( "${sumResult[@]}" "${result[@]}" )
	IFS=' ' result=$(printf '%s\n' "${result[@]}" | parallel  "gcloud container images list --repository={1} --format='get(name)'") 
done

sumResult=( "${sumResult[@]}" "${images[@]}" )
#unset 'sumResult[${#sumResult[@]}-1]'

#change function
if [[ ! -z $1 && $1 == "untagged" ]]; then
	printf '%s\n' "${sumResult[@]}" | parallel "del_untagged {1}"	
else
	if ! [[ $1 =~ $re ]] ; then
   		echo "error: Not a number" >&2; exit 1
	fi
	printf '%s\n' "${sumResult[@]}" | parallel "tags {1} $1"
fi

duration=$(( SECONDS - start ))
echo $duration seconds



