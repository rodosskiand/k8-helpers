#!/bin/bash
array=( $(kubens) )
for t in ${array[@]}; do
  p=$(kubectl get all --namespace $t 2>/dev/null)
  if [[ -z $p  ]]; then
  	ing=$(kubectl get ing --namespace $t 2>/dev/null)
  	if [[ -z $ing  ]]; then
  		echo "$t hasn't ingress"
  	else
  		echo "$t has ingress"	
  		continue
  	fi
  	pvc=$(kubectl get pvc --namespace $t 2>/dev/null)
  	if [[ -z $pvc  ]]; then
  		echo "$t hasn't pvc"
  	else
  		echo "$t has pvc"
  		continue	
  	fi
  	echo "$t is empty"
  	# kubectl delete ns $t
  fi
done

